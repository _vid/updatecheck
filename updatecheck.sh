#!/bin/bash
function fontChange() {
  #ref: https://linuxtidbits.wordpress.com/2008/08/11/output-color-on-bash-scripts/
  #options: normal|success|cmdinfo|info|warning|failure
  txtrst=$(tput sgr0)             # Reset
  txtbld=$(tput bold)             # Bold
  bldcyn=${txtbld}$(tput setaf 6) # Bold blue
  bldylw=${txtbld}$(tput setaf 3) # Bold Yellow
  txtgrn=$(tput setaf 2)           # Green
  bldgrn=${txtbld}${txtgrn}        # Bold Green
  txtred=$(tput setaf 1)           # Red
  bldred=${txtbld}${txtred}        # Bold Red
  txtund=$(tput smul)             # Underline #tput sgr 0 1
  bldund=${txtbld}${txtund}       # Bold Underline


  case "$1" in
    normal)
      echo -en "${txtrst}" > /dev/tty;
      ;;
    success)
      echo -en "${normal}${bldgrn}" > /dev/tty;
    ;;
    cmdinfo)
      echo -en "${normal}${bldcyn}" > /dev/tty;
    ;;
    info)
      echo -en "${normal}${txtgrn}" > /dev/tty;
    ;;
    warning)
      echo -en "${normal}${txtred}" > /dev/tty;
    ;;
    failure)
      echo -en "${normal}${bldred}" > /dev/tty;
    ;;
    underline)
      echo -en "${normal}${txtund}" > /dev/tty;
    ;;
    boldunderline)
      echo -en "${normal}${bldund}" > /dev/tty;
    ;;
    *)
      echo $"Usage: $0 {fontChange normal;|fontChange success;|fontChange cmdinfo;|fontChange info;|fontChange warning;|fontChange failure;|fontChange underline;|fontChange boldunderline;}"
      exit 1
  esac
}

if type drush > /dev/null 2>&1;
then
  # Taken from http://lists.us.dell.com/pipermail/dkms-devel/2004-July/000142.html
  # Got that link from http://stackoverflow.com/questions/4023830/bash-how-compare-two-strings-in-version-format
  version_checker() {
    local ver1=$1
    while [ `echo $ver1 | egrep -c [^0123456789.]` -gt 0 ]; do
      char=`echo $ver1 | sed 's/.*\([^0123456789.]\).*/\1/'`
      char_dec=`echo -n "$char" | od -b | head -1 | awk {'print $2'}`
      ver1=`echo $ver1 | sed "s/$char/.$char_dec/g"`
    done
    local ver2=$2
    while [ `echo $ver2 | egrep -c [^0123456789.]` -gt 0 ]; do
      char=`echo $ver2 | sed 's/.*\([^0123456789.]\).*/\1/'`
      char_dec=`echo -n "$char" | od -b | head -1 | awk {'print $2'}`
      ver2=`echo $ver2 | sed "s/$char/.$char_dec/g"`
    done

    ver1=`echo $ver1 | sed 's/\.\./.0/g'`
    ver2=`echo $ver2 | sed 's/\.\./.0/g'`

    do_version_check "$ver1" "$ver2"
  }
  do_version_check() {

    [ "$1" == "$2" ] && return 10

    ver1front=`echo $1 | cut -d "." -f -1`
    ver1back=`echo $1 | cut -d "." -f 2-`
    ver2front=`echo $2 | cut -d "." -f -1`
    ver2back=`echo $2 | cut -d "." -f 2-`

    if [ "$ver1front" != "$1" ] || [ "$ver2front" != "$2" ]; then
      [ "$ver1front" -gt "$ver2front" ] && return 11
      [ "$ver1front" -lt "$ver2front" ] && return 9

      [ "$ver1front" == "$1" ] || [ -z "$ver1back" ] && ver1back=0
      [ "$ver2front" == "$2" ] || [ -z "$ver2back" ] && ver2back=0
      do_version_check "$ver1back" "$ver2back"
      return $?
    else
      [ "$1" -gt "$2" ] && return 11 || return 9
    fi
  }
  fontChange info; echo "Drush exists: yes"; fontChange normal;
  if [ -f "$1" ]
  then
    fontChange info; echo "Makefile provided: yes"; fontChange normal;
    unset PROJECTUPDATES
    declare -a PROJECTUPDATES
    unset SECURITYUPDATES
    declare -a SECURITYUPDATES
    unset INCLUDES
    declare -a INCLUDES
    unset LIBRARIES
    declare -a LIBRARIES
    unset LIBRARYNAMES
    declare -a LIBRARYNAMES
    LIBRARYNAME=''
    unset NONPRODPROJ
    declare -a NONPRODPROJ
    unset LASTLINE
    LASTLINE=''
    #LIBRARIESLINE: 1 or 0; Track if we found a libraies line most recently and if this line is blank then include it.
    unset LIBRARIESLINE
    LIBRARIESLINE=0
    INCLUDESREGEX="(^includes.*)"
    #ex: includes[drupal_7_uovanilla_base] = "https://git.uoregon.edu/.../tags/d7.43-base-1.0"
    LIBRARIESREGEX="^libraries\[([a-zA-Z0-9\-]+).*"
    COMMENTREGEX="^;.*"
    COREREGEX="core = ([0-9\.x]+)"
    REGEX1="projects\[([a-z_0-9]*)\](\[version\])* = (.*)"
    PROJECTCOUNT=0
    while read line
    do
      if [[ $line =~ $COREREGEX ]]
      then
        VER=${BASH_REMATCH[1]}
        #echo $VER
      fi
      if [[ $line =~ $INCLUDESREGEX ]]
      then
        INCLUDES=("${INCLUDES[@]}" "$line" "")
      fi
      if [[ $line =~ $LIBRARIESREGEX ]]
      then
        LIBRARYNAME="${BASH_REMATCH[1]}"
        if [[ $LASTLINE =~ $COMMENTREGEX ]]
        then
          LIBRARIES=("${LIBRARIES[@]}" "$LASTLINE")
        fi
        LIBRARIES=("${LIBRARIES[@]}" "$line")
        LIBRARYNAMES=("${LIBRARYNAMES[@]}" "$LIBRARYNAME" "")
        LIBRARIESLINE=1
        LIBRARYNAME=''
      else
        #if this line is blank and the last line was a libraries line then include a new line
        if [ $LIBRARIESLINE -eq 1 ] && [ -z "$line" ]
        then
          LIBRARIES=("${LIBRARIES[@]}" "")
        fi
        LIBRARIESLINE=0
      fi
      if [[ $line =~ $REGEX1 ]]
      then
        PROJECTCOUNT=$((PROJECTCOUNT + 1))
        PROJECT=${BASH_REMATCH[1]}
        CURVER=${BASH_REMATCH[3]}
        CURVER=${CURVER//[\"]/}
        REGEX2="(Recommended|Security)"
        PROJNONPRODREGEX="rc|alpha|beta|dev"
        if [ $CURVER == "FALSE" ]
        then
          echo "- Not checking project # $PROJECTCOUNT: projects[$PROJECT] = $CURVER"
        else
          echo "- Checking project # $PROJECTCOUNT: projects[$PROJECT] = $CURVER"
          while read projectline
          do
            if [[ $projectline =~ $REGEX2 ]]
            then
              #echo "$projectline"
              #echo "Current version: $CURVER"
              if [[ $PROJECT == 'drupal' ]]
              then
                #for core ex: 7.41     Security
                REGEX3="([a-z0-9\.\-]+)"
              else
                #ex: 7.x-2.1      Supported, Recommended
                REGEX3="$VER-([a-z0-9\.\-]+)"
              fi
              if [[ $projectline =~ $REGEX3 ]]
              then
                RELEASEVER=${BASH_REMATCH[1]}
                #echo "Release version: $RELEASEVER"
                version_checker $RELEASEVER $CURVER
                if [[ $? -eq 11 ]]
                then
                  #echo ""
                  fontChange warning; echo "** Potential update found for $PROJECT."; fontChange info;
                  echo "$projectline"; fontChange normal;
                  echo ""
                  PROJECTUPDATES=("${PROJECTUPDATES[@]}" "$line -> $RELEASEVER" "")
                  # "#projects[$PROJECT] = $CURVER" "projects[$PROJECT] = $RELEASEVER"
                  REGEX4="(Security)"
                  if [[ $projectline =~ $REGEX4 ]]
                  then
                    SECURITYUPDATES=("${SECURITYUPDATES[@]}" "$line -> $RELEASEVER" "")
                    #"projects[$PROJECT] = $RELEASEVER"
                  fi
                fi
              fi
            fi
            if [[ $CURVER =~ $PROJNONPRODREGEX ]]
            then
              NONPRODPROJ=("${NONPRODPROJ[@]}" "$PROJECT-$VER-$CURVER" "")
            fi
          done < <(drush pm-releases --fields=version,status,date "$PROJECT-$VER-$CURVER")
        fi
      fi
      LASTLINE=$line
    done <$1
    if [[ $((${#INCLUDES[@]} / 2)) > 0 ]]
    then
      echo ""
      fontChange info; fontChange underline; echo "Note included Makefiles (# $((${#INCLUDES[@]} / 2))) were not checked.";
      fontChange normal; fontChange cmdinfo;
      printf '%s\n' "${INCLUDES[@]}"; # print sec. updates on new lines
      fontChange normal;
    fi
    echo ""
    fontChange info; fontChange underline; echo "Total # of projects reviewed: $PROJECTCOUNT. Possible security updates: $((${#SECURITYUPDATES[@]} / 2)). Possible updates: $((${#PROJECTUPDATES[@]} / 2))."; fontChange normal; #/3 because we add 3 elements to the array each time (old project version, suggestion, blank line).

    if [[ $((${#PROJECTUPDATES[@]} / 2)) > 0 ]]
    then
      echo ""
      fontChange warning; fontChange boldunderline; echo "Security updates:"; fontChange normal;
      fontChange warning;
      printf '%s\n' "${SECURITYUPDATES[@]}"; # print sec. updates on new lines
      echo ""
      fontChange info;
      fontChange boldunderline; echo "Possible updates:"; fontChange normal;
      printf '%s\n' "${PROJECTUPDATES[@]}"; # print all possible updates on new lines
    fi
    if [[ $((${#NONPRODPROJ[@]} / 2)) > 0 ]]
    then
      echo "";
      NONPRODPROJ=($(printf "%s\n" "${NONPRODPROJ[@]}" | sort -u));
      fontChange warning; fontChange boldunderline; echo "Non-production Projects (# $((${#NONPRODPROJ[@]}))):"; fontChange normal;
      fontChange warning;
      printf '%s\n' "${NONPRODPROJ[@]}"; # print non prod projects on new lines
    fi
    if [[ $((${#LIBRARYNAMES[@]})) > 0 ]]
    then
      echo "";
      #get only uniq LIBRARYNAMES.
      #Credit: http://stackoverflow.com/questions/13648410/how-can-i-get-unique-values-from-an-array-in-linux-bash
      LIBRARYNAMES=($(printf "%s\n" "${LIBRARYNAMES[@]}" | sort -u));
      fontChange info; fontChange underline; echo "Note besure to check for library updates (# $((${#LIBRARYNAMES[@]}))). List:";
      fontChange normal;
      printf '%s\n' "${LIBRARYNAMES[@]}"; # print LIBRARYNAMES on new lines
      fontChange normal;
      echo "";
      fontChange info; fontChange underline; echo "Library entries found:";
      fontChange normal;
      printf '%s\n' "${LIBRARIES[@]}"; # print sec. updates on new lines
      echo "";
    fi
  else
    fontChange warning; echo "makefile provided: no"; fontChange normal;
  fi
else
  fontChange warning; echo "drush exists: no"; fontChange normal;
fi
