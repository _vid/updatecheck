# Update Check
This is a bash script that checks a drupal .make file for available project updates via (your local installation of) Drush for Modules and Themes, known to drupal.org.
The drush command used is: 
drush pm-releases --fields=version,status,date {project name-version}

## Contact

### Current maintainers:

* Vid Rowan ([\_vid](http://drupal.org/user/631512/))
* Danial Mundra ([dmundra](https://www.drupal.org/u/dmundra))

## Drupal Version Compatiblity

* Drupal 6
* Drupal 7
* Drupal 8 (unconfirmed)

## Purpose
This checks project versions and returns a list of available updates and highlights security releases

## Scope: What this script does

* Loops through each project
    * compares the version specified in the make file w/ the available releases
    * Notifies you if 
        * there is a newer version available
				* there is a newer security release available
* Outputs a list of all available updates
* Outputs a list of available security updates
* Automatically determines the drupal version

## Making Changes

*   If you do make changes consider a fork and pull request

## Requirements

* A make file generated in the format: 
    * projects[{projectname}] = {version.number}
    * ex: projects[extlink] = 1.18
		* Note: this is the standard produced when using 'drush make-generate '

## References

* Ref: Taken from http://lists.us.dell.com/pipermail/dkms-devel/2004-July/000142.html
    * Got that link from http://stackoverflow.com/questions/4023830/bash-how-compare-two-strings-in-version-format

## Repositories

* git.uoregon.edu
    * https://git.uoregon.edu/projects/UO_DRPL_DEV/repos/updatecheck/browse
		* ssh://git@git.uoregon.edu/uo_drpl_dev/updatecheck.git

## Usage

### In short:
* Clone the repo.
    * git clone ssh://git@git.uoregon.edu/uo_drpl_dev/updatecheck.git
* Call the script and reference an existing makefile. Ex:
    *  . updatecheck.sh ~/htdocs/fadev/var_aegir_platforms/makeFiles/prod-637.make