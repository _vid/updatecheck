## 1.3 (2016-05-11)

### New Features

Added code to detect and report on:
 * Non-production module versions eg. rc | alpha | beta | dev
 * libraries
 * make includes

## 1.2 (2016-03-29) #8ee1f36

### New Features

 * adding colors and bold text
 * add font function that works in different flavors of bash


## 1.1 (2016-02-09) #05a87d8

### New Features

 * Added auto-detection check for drupal version; looking for: core = 7.x or similar.

## 1.0 (2016-02-03) #b3e9ed0

Extracted code from uovanilla_makefiles.git
